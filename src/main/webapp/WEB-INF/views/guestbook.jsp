<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Guest Book</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
html, body, h1, h2, h3, h4, h5, h6 {
	font-family: "Comic Sans MS", cursive, sans-serif;
}
</style>
</head>

<body>
	<div class="w3-container w3-section">
		<div class="w3-container">
			<div class="w3-container w3-blue w3-margin-bottom">
				<h2>Guest Book</h2>
			</div>
			<form:form action="/guestbook" method="post"
				modelAttribute="guestBook">
				<div class="form-group">
					<form:label path="name">Name:</form:label>
					<form:input class="w3-input w3-margin-bottom" path="name"
						placeholder="Enter name" />
				</div>
				<div class="form-group">
					<form:label path="comment">Comments:</form:label>
					<form:input path="comment" class="w3-input w3-margin-bottom"
						placeholder="Enter comments" />
				</div>
				<div>
					<input type="submit" class="w3-btn w3-blue w3-margin-bottom"
						value="Submit" />
				</div>
			</form:form>

			<c:if test="${not empty guestbooks}">
				<table class="w3-table-all w3-card-4">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Name</th>
							<th scope="col">Comments</th>
							<th scope="col">Created Date</th>
						</tr>
					</thead>
					<tbody>

						<c:forEach var="guestbook" items="${guestbooks}"
							varStatus="status">
							<tr>
								<th scope="row">${status.count}</th>
								<td><c:out value="${guestbook.name}" /></td>
								<td><c:out value="${guestbook.comment}" /></td>
								<td><c:out value="${guestbook.createdDate}" /></td>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</c:if>
		</div>
	</div>

</body>
</html>