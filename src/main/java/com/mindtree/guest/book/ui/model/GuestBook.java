package com.mindtree.guest.book.ui.model;

import lombok.Data;

@Data
public class GuestBook {
		
	private int id;
	private String name;
	private String comment;
	private String createdDate;

}
