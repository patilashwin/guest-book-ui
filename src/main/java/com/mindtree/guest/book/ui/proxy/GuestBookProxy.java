package com.mindtree.guest.book.ui.proxy;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mindtree.guest.book.ui.model.GuestBook;

@FeignClient(name = "guest-book", url = "${guest.book.api.url}")
public interface GuestBookProxy {
	
	@GetMapping
	public List<GuestBook> getAllGuestBook();
	
	@PostMapping
	public GuestBook save(@RequestBody GuestBook guestBook);

}
