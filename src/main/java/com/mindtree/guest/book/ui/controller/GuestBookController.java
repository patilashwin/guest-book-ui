package com.mindtree.guest.book.ui.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mindtree.guest.book.ui.model.GuestBook;
import com.mindtree.guest.book.ui.proxy.GuestBookProxy;

@Controller
public class GuestBookController {

	@Autowired
	GuestBookProxy proxy;

	@RequestMapping("/")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("guestbook");
		modelAndView.addObject("guestBook", new GuestBook());
		modelAndView.addObject("guestbooks", proxy.getAllGuestBook());
		return modelAndView;
	}
	
	@PostMapping("/guestbook")
	public ModelAndView guestBook(GuestBook guestBook, Model model) {
		proxy.save(guestBook);
		List<GuestBook> allGuestBook = proxy.getAllGuestBook();
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("guestbook");
		modelAndView.addObject("guestbooks", allGuestBook);
		return modelAndView;
	}

}
