FROM openjdk:8-jre
VOLUME /tmp
WORKDIR /data
COPY target/guest-book-ui.war  /data
EXPOSE 40001
ENTRYPOINT ["java" ,"-jar", "guest-book-ui.war"]